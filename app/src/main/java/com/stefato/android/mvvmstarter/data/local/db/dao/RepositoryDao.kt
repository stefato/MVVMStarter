package com.stefato.android.mvvmstarter.data.local.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.stefato.android.mvvmstarter.data.local.db.entity.RepositoryEntity
import io.reactivex.Flowable


@Dao
interface RepositoryDao {
    @Query("SELECT * from repository")
    fun getAll(): List<RepositoryEntity>

    @Insert(onConflict = REPLACE)
    fun insert(repositoryEntity: RepositoryEntity)

    @Query("DELETE from repository")
    fun deleteAll()

    @Query("SELECT EXISTS(SELECT 1 FROM repository WHERE name=:name AND owner=:owner)")
    fun entryExists(name: String, owner: String): Boolean

    @Query("SELECT * FROM repository WHERE name=:name AND owner=:owner LIMIT 1")
    fun findByNameAndOwner(name: String, owner: String): Flowable<List<RepositoryEntity>>
}