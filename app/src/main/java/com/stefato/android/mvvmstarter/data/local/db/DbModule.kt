package com.stefato.android.mvvmstarter.data.local.db

import android.app.Application
import android.arch.persistence.room.Room
import com.stefato.android.mvvmstarter.data.local.db.config.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    val DB_NAME = "starter-db"

    @Provides
    @Singleton
    fun provideRoomDataBase(application: Application) =
            Room.databaseBuilder(application, AppDatabase::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
}