package com.stefato.android.mvvmstarter.data.remote

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class ServiceModule {

    private val baseUrl = "https://api.github.com/"
    //private val testToken = ""
    private val loggingLevel = HttpLoggingInterceptor.Level.BASIC

    @Provides
    @Singleton
    fun loggingInterceptor() = HttpLoggingInterceptor().apply { level = loggingLevel }

    @Provides
    @Singleton
    fun okhttp(logging: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(logging)
                    //.addInterceptor(auth)
                    .build()


    //simple header auth
    /*
    @Provides
     @Singleton
     fun authInterceptor() = Interceptor { chain ->
         val original = chain.request()
         val request = original.newBuilder()
                 .addHeader("Authorization", testToken)
                 .method(original.method(), original.body())
                 .build()
         chain.proceed(request)
     }

     */

    @Provides
    @Singleton
    fun retrofit(client: OkHttpClient, moshi: Moshi): Retrofit =
            Retrofit.Builder()
                    .client(client)
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            MoshiConverterFactory.create(moshi))
                    .baseUrl(baseUrl)
                    .build()

    @Provides
    @Singleton
    fun githubService(retrofit: Retrofit): GithubService = retrofit.create(GithubService::class.java)

}