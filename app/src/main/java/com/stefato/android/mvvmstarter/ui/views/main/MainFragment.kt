package com.stefato.android.mvvmstarter.ui.views.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.stefato.android.mvvmstarter.R
import com.stefato.android.mvvmstarter.ui.views.base.BaseFragment
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : BaseFragment<MainViewModel>() {

    override val classToken = MainViewModel::class.java

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_button.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_listFragment))
    }
}
