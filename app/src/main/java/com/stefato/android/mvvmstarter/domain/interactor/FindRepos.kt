package com.stefato.android.mvvmstarter.domain.interactor

import com.stefato.android.mvvmstarter.domain.repo.GitHubRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FindRepos @Inject constructor(private val repository: GitHubRepo) {

    fun find(keyword: String = "") =
            if (keyword == "") repository.getAllRepos()
            else repository.searchRepos(keyword)
}