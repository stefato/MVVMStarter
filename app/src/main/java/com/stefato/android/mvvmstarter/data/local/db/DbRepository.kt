package com.stefato.giteaclient.data.local.database

import com.stefato.android.mvvmstarter.data.local.db.config.AppDatabase
import com.stefato.android.mvvmstarter.data.local.db.entity.RepositoryEntity
import com.stefato.android.mvvmstarter.domain.model.Repository
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DbRepository @Inject constructor(database: AppDatabase) {

    private val repositoryDao = database.repositoryDao()

    fun getAll() = repositoryDao.getAll()

    fun insert(repository: Repository): Observable<Boolean> =
            Observable.fromCallable {
                repositoryDao.insert(repository.toRepositoryEntity())
            }.map { true }

    fun deleteAll() = repositoryDao.deleteAll()

    private fun Repository.toRepositoryEntity() =
            RepositoryEntity(name, owner.login, description, stars)
}


