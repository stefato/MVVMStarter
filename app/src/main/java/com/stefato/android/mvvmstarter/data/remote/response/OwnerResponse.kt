package com.stefato.android.mvvmstarter.data.remote.response

import com.squareup.moshi.Json

data class OwnerResponse(
        @Json(name = "login")
        var login: String? = null
)