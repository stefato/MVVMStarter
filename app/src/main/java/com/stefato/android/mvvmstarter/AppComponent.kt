package com.stefato.android.mvvmstarter

import com.stefato.android.mvvmstarter.data.DataModule
import com.stefato.android.mvvmstarter.ui.UiModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class,
    AppModule::class,
    UiModule::class,
    DataModule::class
])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}