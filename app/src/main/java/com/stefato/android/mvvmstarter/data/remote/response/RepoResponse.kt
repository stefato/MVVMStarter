package com.stefato.android.mvvmstarter.data.remote.response

import com.squareup.moshi.Json

data class RepoResponse(
        @Json(name = "id")
        var id: Int? = null,
        @Json(name = "name")
        var name: String? = null,
        @Json(name = "full_name")
        var fullName: String? = null,
        @Json(name = "owner")
        var owner: OwnerResponse? = null,
        @Json(name = "description")
        var description: String? = null,
        @Json(name = "stargazers_count")
        var stargazersCount: Int? = null
)