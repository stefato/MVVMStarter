package com.stefato.android.mvvmstarter.data

import com.squareup.moshi.Moshi
import com.stefato.android.mvvmstarter.data.local.db.DbModule
import com.stefato.android.mvvmstarter.data.remote.ServiceModule
import com.stefato.android.mvvmstarter.domain.repo.GitHubRepo
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Suppress("unused")
@Module(includes = [ServiceModule::class,DbModule::class])
abstract class DataModule {

    @Module
    companion object {
        @Provides
        @Singleton
        @JvmStatic
        fun provideMoshi(): Moshi = Moshi.Builder().build()
    }

    @Binds
    abstract fun bindRepoRepository(repository: GitHubDataRepo): GitHubRepo
}