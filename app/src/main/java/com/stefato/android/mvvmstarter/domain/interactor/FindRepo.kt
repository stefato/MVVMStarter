package com.stefato.android.mvvmstarter.domain.interactor

import com.stefato.android.mvvmstarter.domain.repo.GitHubRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FindRepo @Inject constructor(private val repository: GitHubRepo) {

    fun find(name: String, user: String) = repository.getRepo(name, user)
}