package com.stefato.android.mvvmstarter.ui.views.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.Fragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment<V : ViewModel> : Fragment() {

    //we need to know the view model type at compile time
    //so all derived classes have to declare it
    abstract val classToken: Class<V>

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    protected val viewModel: V by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(classToken)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}