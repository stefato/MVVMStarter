package com.stefato.android.mvvmstarter.ui.views.util

const val REPO_NAME_KEY = "repoName"
const val REPO_OWNER_KEY = "repoOwner"