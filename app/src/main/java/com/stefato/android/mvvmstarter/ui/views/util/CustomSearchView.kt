package com.stefato.android.mvvmstarter.ui.views.util

import android.content.Context
import android.support.v7.widget.SearchView
import android.util.AttributeSet

class CustomSearchView : SearchView {

    var searchSrcTextView: SearchView.SearchAutoComplete? = null
    var listener: SearchView.OnQueryTextListener? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setOnQueryTextListener(listener: OnQueryTextListener?) {
        super.setOnQueryTextListener(listener)
        this.listener = listener
        searchSrcTextView = this.findViewById(android.support.v7.appcompat.R.id.search_src_text)
        searchSrcTextView?.apply {
            setOnEditorActionListener { _, _, _ ->
                listener?.onQueryTextSubmit(query.toString())
                true
            }
        }
    }
}