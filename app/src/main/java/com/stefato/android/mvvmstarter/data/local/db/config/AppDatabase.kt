package com.stefato.android.mvvmstarter.data.local.db.config

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.stefato.android.mvvmstarter.data.local.db.dao.RepositoryDao
import com.stefato.android.mvvmstarter.data.local.db.entity.RepositoryEntity

@Database(entities = [RepositoryEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun repositoryDao(): RepositoryDao
}