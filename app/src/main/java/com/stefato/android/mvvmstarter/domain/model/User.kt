package com.stefato.android.mvvmstarter.domain.model

data class User(
        val login: String = ""
)