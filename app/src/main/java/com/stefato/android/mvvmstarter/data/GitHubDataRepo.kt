package com.stefato.android.mvvmstarter.data

import com.stefato.android.mvvmstarter.data.remote.GithubService
import com.stefato.android.mvvmstarter.data.remote.response.OwnerResponse
import com.stefato.android.mvvmstarter.data.remote.response.RepoResponse
import com.stefato.android.mvvmstarter.domain.model.Repository
import com.stefato.android.mvvmstarter.domain.model.User
import com.stefato.android.mvvmstarter.domain.repo.GitHubRepo
import com.stefato.giteaclient.data.local.database.DbRepository
import io.reactivex.Observable
import javax.inject.Inject

class GitHubDataRepo @Inject constructor(
        private val githubService: GithubService,
        private val dbRepo: DbRepository
) : GitHubRepo {

    override fun saveRepo(repo: Repository) = dbRepo.insert(repo)

    override fun getRepo(name: String, user: String): Observable<Repository> =
            githubService.getRepo(user, name)
                    .map { it.toRepository() }

    override fun searchRepos(keyword: String): Observable<List<Repository>> =
            githubService.searchRepos(keyword)
                    .map { it.items?.toRepositoryList() ?: listOf() }

    override fun getAllRepos(): Observable<List<Repository>> =
            githubService.getAllRepos()
                    .map {
                        //TODO possible way to handle status codes, not exhaustive just an example
                        when(it.code()){
                            in 200..299 ->it.body()?.toRepositoryList()?: listOf()
                            else -> listOf()
                        }
                    }

    //private extension functions can just be used in the same file
    private fun List<RepoResponse>.toRepositoryList() = map { it.toRepository() }

    private fun RepoResponse.toRepository(): Repository {
        val id = id
        val name = name
        val description = description ?: ""
        val owner = owner?.run { toUser() } ?: User()
        val stars = stargazersCount ?: 0

        return if (id != null && name != null) {
            Repository(id, name, description, owner, stars)
        } else Repository()
    }

    private fun OwnerResponse.toUser() = User(login ?: "")
}


