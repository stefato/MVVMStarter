package com.stefato.android.mvvmstarter.ui.views.detail

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.stefato.android.mvvmstarter.R
import com.stefato.android.mvvmstarter.ui.views.base.BaseFragment
import com.stefato.android.mvvmstarter.ui.views.detail.DetailViewModel.UiModel.*
import com.stefato.android.mvvmstarter.ui.views.util.REPO_NAME_KEY
import com.stefato.android.mvvmstarter.ui.views.util.REPO_OWNER_KEY
import kotlinx.android.synthetic.main.detail_fragment.*

class DetailFragment : BaseFragment<DetailViewModel>() {

    override val classToken = DetailViewModel::class.java

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInteraction()
        handleArguments()
    }

    private fun initInteraction() {
        viewModel.data.observe(viewLifecycleOwner,
                Observer { uiModel -> uiModel?.let { handleUiModel(it) } })
    }

    private fun handleArguments() {
        arguments?.takeIf { it.containsKey(REPO_NAME_KEY) && it.containsKey(REPO_OWNER_KEY) }
                ?.apply {
                    viewModel.loadRepo(getString(REPO_NAME_KEY), getString(REPO_OWNER_KEY))
                }
    }

    //return when to make use of sealed class
    private fun handleUiModel(uiModel: DetailViewModel.UiModel) =
        when (uiModel) {
            is Loading -> showLoading()
            is Success -> show(uiModel)
            is Error -> showError(uiModel.cause)
        }


    private fun showLoading() {
        //TODO implement
    }

    private fun show(uiModel: Success) {
        with(uiModel.repo) {
            title.text = name
            user.text = owner.login
            starCount.text = stars.toString()
            ownerName.text = description
        }
    }

    private fun showError(cause: Throwable) {
        loadingFinished()
        Toast.makeText(requireActivity(), cause.message ?: "", Toast.LENGTH_LONG).show()
    }

    private fun loadingFinished() {
        //TODO implement
    }

}
