package com.stefato.android.mvvmstarter.domain.model

data class Repository(
        val id: Int = 0,
        val name: String  = "",
        val description: String = "",
        val owner: User =User(""),
        val stars: Int = 0
)
