package com.stefato.android.mvvmstarter.ui.injection

import com.stefato.android.mvvmstarter.ui.views.detail.DetailFragment
import com.stefato.android.mvvmstarter.ui.views.list.ListFragment
import com.stefato.android.mvvmstarter.ui.views.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

//register all fragments here
@Suppress("unused")
@Module
abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeListFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailFragment(): DetailFragment
}