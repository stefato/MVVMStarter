package com.stefato.android.mvvmstarter.domain.interactor

import com.stefato.android.mvvmstarter.domain.model.Repository
import com.stefato.android.mvvmstarter.domain.repo.GitHubRepo
import javax.inject.Inject
import javax.inject.Singleton


//unused - just an example
@Singleton
class SaveRepo @Inject constructor(private val repository: GitHubRepo) {

    fun save(repo: Repository) = repository.saveRepo(repo)
}