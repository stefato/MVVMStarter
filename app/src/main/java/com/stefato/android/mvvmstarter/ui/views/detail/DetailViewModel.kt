package com.stefato.android.mvvmstarter.ui.views.detail

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.stefato.android.mvvmstarter.domain.interactor.FindRepo
import com.stefato.android.mvvmstarter.domain.model.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val findRepo: FindRepo) : ViewModel() {

    val data: MutableLiveData<UiModel> = MutableLiveData()

    private val disposables = CompositeDisposable()

    fun loadRepo(repoName: String, repoOwner: String) {
        val disposable = findRepo.find(repoName, repoOwner).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .map { UiModel.Success(it) as UiModel }
                .startWith(UiModel.Loading)
                .onErrorReturn { UiModel.Error(it) }
                .subscribe {
                    data.value = it
                }

        disposables.add(disposable)
    }

    sealed class UiModel {
        object Loading : UiModel()
        data class Success(val repo: Repository) : UiModel()
        data class Error(val cause: Throwable) : UiModel()
    }
}
