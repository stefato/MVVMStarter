package com.stefato.android.mvvmstarter.data.remote.response

import com.squareup.moshi.Json


class SearchResponse {
    @Json(name = "items")
    var items: List<RepoResponse>? = null
}