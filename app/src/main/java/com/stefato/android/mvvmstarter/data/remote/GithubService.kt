package com.stefato.android.mvvmstarter.data.remote

import com.stefato.android.mvvmstarter.data.remote.response.RepoResponse
import com.stefato.android.mvvmstarter.data.remote.response.SearchResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("repos/{owner}/{name}")
    fun getRepo(
            @Path("owner") owner: String,
            @Path("name") name: String
    ): Observable<RepoResponse>

    @GET("repositories")
    fun getAllRepos(): Observable<Response<List<RepoResponse>>>

    @GET("search/repositories")
    fun searchRepos(
            @Query("q") query: String,
            @Query("page") page: Int = 1
    ): Observable<SearchResponse>
}