package com.stefato.android.mvvmstarter.domain.repo

import com.stefato.android.mvvmstarter.domain.model.Repository
import io.reactivex.Observable

interface GitHubRepo {

    fun getRepo(name: String, user: String): Observable<Repository>

    fun getAllRepos(): Observable<List<Repository>>

    fun searchRepos(keyword: String): Observable<List<Repository>>

    fun saveRepo(repo:Repository): Observable<Boolean>
}