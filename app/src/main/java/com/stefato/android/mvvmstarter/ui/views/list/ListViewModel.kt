package com.stefato.android.mvvmstarter.ui.views.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.stefato.android.mvvmstarter.domain.interactor.FindRepos
import com.stefato.android.mvvmstarter.domain.model.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class ListViewModel @Inject constructor(private val findRepos: FindRepos) : ViewModel() {

    val data: MutableLiveData<UiModel> = MutableLiveData()

    private val disposables = CompositeDisposable()

    fun loadRepos(keyword: String = "") {
        val disposable = findRepos.find(keyword).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .map { UiModel.Success(it) as UiModel }
                .startWith(UiModel.Loading)
                .onErrorReturn { UiModel.Error(it) }
                .subscribe {
                    data.value = it
                }

        disposables.add(disposable)
    }

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }

    sealed class UiModel {
        object Loading : UiModel()
        data class Success(val list: List<Repository>) : UiModel()
        data class Error(val cause: Throwable) : UiModel()
    }
}

