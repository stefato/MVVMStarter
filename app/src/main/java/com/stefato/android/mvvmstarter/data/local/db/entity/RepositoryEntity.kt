package com.stefato.android.mvvmstarter.data.local.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index

@Entity(tableName = "repository",
        indices = [Index(value = ["name", "owner"], unique = true)],
        primaryKeys = ["name", "owner"])
data class RepositoryEntity(
        var name: String,
        var owner: String,
        val description: String,
        val stars: Int = 0
)