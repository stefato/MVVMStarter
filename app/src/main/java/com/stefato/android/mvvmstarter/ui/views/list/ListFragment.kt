package com.stefato.android.mvvmstarter.ui.views.list

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.jakewharton.rxbinding2.support.v7.widget.queryTextChangeEvents
import com.stefato.android.mvvmstarter.R
import com.stefato.android.mvvmstarter.domain.model.Repository
import com.stefato.android.mvvmstarter.ui.views.base.BaseFragment
import com.stefato.android.mvvmstarter.ui.views.list.ListViewModel.UiModel.*
import com.stefato.android.mvvmstarter.ui.views.util.REPO_NAME_KEY
import com.stefato.android.mvvmstarter.ui.views.util.REPO_OWNER_KEY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.list_fragment.*

class ListFragment : BaseFragment<ListViewModel>() {

    override val classToken = ListViewModel::class.java

    private val viewAdapter = ListAdapter()

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInteraction()
        initRecyclerView()
        initSearch()

        if (savedInstanceState == null) {
            viewModel.loadRepos()
            repo_search_view.apply {
                setQuery("",false)
                isIconified = true
            }
        }
    }

    private fun initInteraction() {
        viewModel.data.observe(viewLifecycleOwner,
                Observer { uiModel -> uiModel?.let { handleUiModel(it) } })
    }

    private fun initRecyclerView() {
        val viewManager = LinearLayoutManager(context)

        list_rv.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
            addItemDecoration(DividerItemDecoration(context, viewManager.orientation))
        }

        viewAdapter.onClick = { onRepoClick(it) }
    }

    private fun initSearch() {
        val disposable = repo_search_view.queryTextChangeEvents()
                //.debounce(DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.isSubmitted)
                        viewModel.loadRepos(it.queryText().toString())
                }

        disposables.add(disposable)
    }

    //return when to make use of sealed class
    private fun handleUiModel(uiModel: ListViewModel.UiModel) =
            when (uiModel) {
                is Loading -> showLoading()
                is Success -> show(uiModel)
                is Error -> showError(uiModel.cause)
            }


    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    private fun show(uiModel: Success) {
        loadingFinished()
        viewAdapter.setList(uiModel.list)
    }

    private fun showError(cause: Throwable) {
        loadingFinished()
        Toast.makeText(requireActivity(), cause.message ?: "", Toast.LENGTH_LONG).show()
    }

    private fun loadingFinished() {
        progressBar.visibility = View.INVISIBLE
    }

    private fun onRepoClick(repo: Repository) {
        val bundle = Bundle().apply {
            putString(REPO_NAME_KEY, repo.name)
            putString(REPO_OWNER_KEY, repo.owner.login)
        }
        findNavController().navigate(R.id.action_listFragment_to_detailFragment, bundle)
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }
}
