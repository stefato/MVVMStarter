package com.stefato.android.mvvmstarter.ui.views.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.stefato.android.mvvmstarter.R
import com.stefato.android.mvvmstarter.domain.model.Repository
import kotlinx.android.synthetic.main.list_text_view.view.*

class ListAdapter : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    private var repoList = listOf<Repository>()

    //TODO pass listener in constructor?
    var onClick: (Repository) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val textView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_text_view, parent, false)

        val holder = ViewHolder(textView)

        holder.itemView.setOnClickListener { onClick(repoList[holder.adapterPosition]) }

        return holder
    }

    fun setList(list: List<Repository>) {
        //TODO sometimes it will be better to diff
        this.repoList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = repoList[position]
        with(holder) {
            title.text = repo.name
            ownerName.text = repo.owner.login
            starsCount.text = repo.stars.toString()
        }
    }

    override fun getItemCount() = repoList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //cache view in viewHolder or else findViewById()
        // could be called multiple times during scrolling etc
        //https://medium.com/redso/trap-in-kotlin-android-extensions-d07be00759fa
        val title: TextView = itemView.title
        val ownerName: TextView = itemView.ownerName
        val starsCount: TextView = itemView.starsCount
    }
}