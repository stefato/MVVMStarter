package com.stefato.android.mvvmstarter.ui.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.stefato.android.mvvmstarter.ui.views.util.ViewModelFactory
import com.stefato.android.mvvmstarter.ui.views.util.ViewModelKey
import com.stefato.android.mvvmstarter.ui.views.detail.DetailViewModel
import com.stefato.android.mvvmstarter.ui.views.list.ListViewModel
import com.stefato.android.mvvmstarter.ui.views.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

// register all view models here
@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(vm: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindListViewModel(vm: ListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(vm: DetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}