package com.stefato.android.mvvmstarter

import android.app.Application
import dagger.Binds
import dagger.Module
import javax.inject.Singleton


@Suppress("unused")
@Module
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun application(app: App): Application

    /*@Module
    companion object {
        @Provides
        @Singleton
        @JvmStatic
        @Named("ANDROID_CACHE_DIR")
        fun provideCacheDir(app: Application): File? = app.cacheDir

        @Provides
        @Singleton
        @JvmStatic
        @Named("ANDROID_FILES_DIR")
        fun provideFileDir(app: Application): File? = app.filesDir
    }
    */
}