package com.stefato.android.mvvmstarter

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.github.ajalt.timberkt.Timber
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = activityInjector

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        DaggerAppComponent.builder().create(this).inject(this)

        // Enable RxJava assembly stack collection, to make RxJava crash reports clear and unique
        // Make sure this is called AFTER setting up any Crash reporting mechanism as Crashlytics
        //RxJava2Debug.enableRxJava2AssemblyTracking(arrayOf("com.stefato.giteaclient"))
    }
}

