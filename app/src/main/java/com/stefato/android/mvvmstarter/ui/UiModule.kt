package com.stefato.android.mvvmstarter.ui

import com.stefato.android.mvvmstarter.ui.injection.FragmentsModule
import com.stefato.android.mvvmstarter.ui.injection.ViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module(includes = [ViewModelModule::class])
abstract class UiModule {

    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun contributeMainActivity(): MainActivity

}